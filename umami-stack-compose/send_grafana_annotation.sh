#!/bin/bash

grafana_api_url="$1"
grafana_api_token="$2"

env="$3"
tezos_net="$4"
action="$5"

text="Event - Manual action ($env - $tezos_net): $action"

time_epoch_ms=$(date +%s%3N)

payload=$(cat <<EOM
{
  "time":$time_epoch_ms,
  "tags":["manual_op","$env","$tezos_net"],
  "text":"$text"
}
EOM
)
echo "$payload"

#https://grafana.com/docs/grafana/latest/http_api/annotations/#create-annotation

curl -w "\n" -X POST \
	-H 'Accept: application/json' \
	-H 'Content-type: application/json' \
	-H "Authorization: Bearer $grafana_api_token" \
	--data "$payload"  "$grafana_api_url/api/annotations"
