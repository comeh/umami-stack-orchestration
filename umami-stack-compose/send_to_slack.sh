#!/bin/bash -x
if [[ -z $slack_webhook_url ]]
then
     slack_webhook_url=${3}
fi

who=$(whoami)

slack_channel=${1:-perso-test}
text=${2:-test}


payload="payload={\"channel\": \"$slack_channel\", \"username\": \"webhook\", \"text\": \"$text by $who\", \"icon_emoji\": \":ghost:\"}"
curl -w "\n" -X POST --data-urlencode "$payload" "$slack_webhook_url"
