#!/bin/bash

###  create directory for logs (once)

# sudo mkdir /var/log/umami-stack/
## sudo chmod 777 /var/log/umami-stack/
### sudo chown tezos:tezos /var/log/umami-stack/
### sudo su tezos
# crontab -e

### run every minute from 01/april to 02/april, line depends on the path.
# * * 01-02 04 * /opt/umami-stack/amino/config/upg_mainnet_ithaca.sh >> /var/log/umami-stack/upg_mainnet_ithaca 2>&1
# * * 01-02 04 * /opt/umami-backend/amino/config/upg_mainnet_ithaca.sh  >> /var/log/umami-stack/upg_mainnet_ithaca 2>&1
# server qa
# * * 01-02 04 * /opt/umami-stack/amino/umami-stack-orchestration/umami-stack-compose/upg_mainnet_ithaca.sh >> /var/log/umami-stack/upg_mainnet_ithaca 2>&1
# new servers :
# * * 01-2 04 * /opt/umami/umami-stack/amino/umami-stack-orchestration/umami-stack-compose/upg_mainnet_ithaca.sh  >> /var/log/umami-stack/upg_mainnet_ithaca 2>&1

### observe the logs
# tail -f /var/log/umami-stack/upg_mainnet_ithaca

echo "$(date) - $0 -- start test"
# for env in mainnet
# do

env="mainnet"
amino_path="/opt/umami/umami-stack/amino"
echo "$(date) - $0 - $env"

make -f ${amino_path}/${env}/Makefile -C ${amino_path}/${env}/ upg_mezos_ithaca 2>&1
sudo docker inspect ${env}-mezos | grep MEZOS_PROTO

# done
echo "$(date) - $0 -- end test"
