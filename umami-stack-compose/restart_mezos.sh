#!/bin/bash

###  create directory for logs (once)

# sudo mkdir /var/log/umami-stack/
## sudo chmod 777 /var/log/umami-stack/
### sudo chown tezos:tezos /var/log/umami-stack/
### sudo su tezos
# crontab -e

# ithaca (mix of new and old servers)
### run every minute from 01/april to 02/april, line depends on the path.
# * * 01-02 04 * /opt/umami-stack/amino/config/upg_mainnet_mezos.sh >> /var/log/umami-stack/upg_mainnet_mezos 2>&1
# * * 01-02 04 * /opt/umami-backend/amino/config/upg_mainnet_mezos.sh  >> /var/log/umami-stack/upg_mainnet_mezos 2>&1
# server qa
# * * 01-02 04 * /opt/umami-stack/amino/umami-stack-orchestration/umami-stack-compose/upg_mainnet_mezos.sh >> /var/log/umami-stack/upg_mainnet_mezos 2>&1
# new servers :
# * * 01-2 04 * /opt/umami/umami-stack/amino/umami-stack-orchestration/umami-stack-compose/upg_mainnet_mezos.sh  >> /var/log/umami-stack/upg_mainnet_mezos 2>&1

#(new servers)  kathmandu on ghostnet
# */5 * 16-17 09 * /opt/umami/umami-stack/amino/umami-stack-orchestration/umami-stack-compose/restart_mezos.sh ghostnet >> /var/log/umami-stack/upg_ghostnet_mezos 2>&1
#*/5 * 23-24 09 * /opt/umami/umami-stack/amino/umami-stack-orchestration/umami-stack-compose/restart_mezos.sh mainnet >> /var/log/umami-stack/upg_mainnet_mezos 2>&2

### observe the logs
# tail -f /var/log/umami-stack/upg_mainnet_mezos

echo "$(date) - $0 -- start test"
echo
tezos_net="$1"
amino_path="/opt/umami/umami-stack/amino"

echo "$(date) - $0 - $env"

#make -f ${amino_path}/${env}/Makefile -C ${amino_path}/${env}/ upg_mezos 2>&1
#sudo docker inspect ${env}-mezos | grep MEZOS_PROTO
export SLACK_REPEAT="no"

#make -f ${amino_path}/${tezos_net}/Makefile -C ${amino_path}/${env}/ version 2>&1
source ${amino_path}/${tezos_net}/.env

echo "----- curl"
curl -s http://localhost:${mezos_port_rpc}/version 2>&1

echo "----- restart-mezos"
make -f ${amino_path}/${tezos_net}/Makefile -C ${amino_path}/${tezos_net}/ restart-mezos 2>&1

# done
echo
echo "$(date) - $0 -- end test"

