#!/bin/bash 

source .env
NODE_LEVEL=$(curl -s http://localhost:${node_port_rpc}/chains/main/blocks/head | jq .header.level)
IDXR_LEVEL=$(docker exec -it ${tezos_net}-postgres-1 psql -U indexer_dbuser indexer_db -c 'select count(*) from c.block;' -t| tr -dc '[:alnum:]\n' )
DRIFT=$(( ${NODE_LEVEL} - ${IDXR_LEVEL} ))

now_epoch=$(date '+%s')
printf "Node level :    %s\n" $NODE_LEVEL
printf "Indexed level : %s\n" $IDXR_LEVEL
printf "     Drift :    %s\n" $DRIFT
printf " (as of %s)\n" "$(date)"

printf "%s,%s\n" $DRIFT $now_epoch | tee -a bloc_epoch.txt


awk=/usr/bin/awk

awkcommand='
BEGIN { FS = "[ ,\t]+" }
NF == 2 { x_sum += $1
          y_sum += $2
          xy_sum += $1*$2
          x2_sum += $1*$1
          num += 1
          x[NR] = $1
          y[NR] = $2
        }
END { mean_x = x_sum / num
      mean_y = y_sum / num
      mean_xy = xy_sum / num
      mean_x2 = x2_sum / num
      slope = (mean_xy - (mean_x*mean_y)) / (mean_x2 - (mean_x*mean_x))
      inter = mean_y - slope * mean_x
      for (i = num; i > 0; i--) {
          ss_total += (y[i] - mean_y) ^ 2
          ss_residual += (y[i] - (slope * x[i] + inter)) ^ 2
      }
      r2 = 1 - (ss_residual / ss_total)
      printf("Slope      :  %g\n", slope)
      printf("R-Squared  :  %g\n", r2)
      printf("Intercept  :  %f\n", inter)
      printf("Intercept date : %s\n", strftime("%c",int(inter)))
    }
'

$awk "$awkcommand" bloc_epoch.txt
